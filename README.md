# RBAC
Role Based Access Control


## Installation

```bash
$ npm install @parthar/rbac --save
```

## Overview
Role Based Access Control (also knwon as RBAC) provides a means for information security by managing access to resources via roles assigned to users.

[Permissions](#permissions-api) are actions that can be performed on resource(s). A "resource" is anything whose access needs to be controlled/moderated. An "action" is anything that can be performed on the resource. For e.g., "password" could be a resource, and a user could perform multiple actions on it like: "verify", "change", and "reset". 

[Grants](#grants-api) is the process of associating a group-of permissions to a role. Each "role" assigns a collection of permissions to a user so as to model the different roles and responsibilities within an organization. For e.g., an "admin" role can be allowed to perform all actions on available resources, while a "user" role can be restricted to only normal user operations.

[Store](#store-api) is the persistence back-end for roles/permissions. The default implementation stores data in-memory. However, it is possible to extend this and implement it for any back-end as needed.

This module provides the following features:
* Chainable/Fluent API with both callback & promise interfaces
* Roles inheritance & allow/deny permissions
* Glob notation for resources & permissions
* Interfaces can also be used within a browser front-end
* Separate "store" interface to tie to any persistance model


## Usage

```js
// create permissions
var userPermissions = new Permissions();
userPermissions.resource("userprofile").actions(["read", "update"]) // permissions to read/update userprofile
    .resource("password").actions(["verify", "change", "reset"]) // permissions to verify/change/reset password
var adminPermissions = new Permissions();
adminPermissions.resource("userprofile").actions(["create", "delete"]) // permissions to create/delete userprofile
    .resource("password").actions(["reset"]); // permissions to verify/change/reset password
// allow all actions against a resource using glob-notation
perms.resource("device").actions(["*"]);
// allow all actions against all resources using glob-notation
perms.resource("*").actions(["*"]);
// list resources & actions
userPermissions.listResources(); // ["userprofile", "password"]
userPermissions.actions4resource("userprofile"); // ["read", "update"]
// export/import permissions to/from store
var stringsArray = userPermissions.export(); // array of strings
userPermissions = new Permissions(stringsArray);
// check if a permission-obj is allowed to perform an action on a resource
userPermissions.can("password", "change"); // true
adminPermissions.can("password", "change"); // false

// grant roles
var store = new Store(); // or any other store that extends this
var grants = new Grants(store);
grants.role("user").allow(userPerms).commit();
grants.role("admin").allow(adminPerms).commit();
// can inherit roles and allow/deny permissions
grants.role("other").inherit("user").allow(extraPerms).deny(unwantedPerms).commit();
// list available roles & permissions
grants.listRoles(); // ["user", "admin", "other"]
grants.permissions4role("user"); // userPermissions object
// delete a role
grants.deleteRole("user");
// check if one or more roles is allowed to perform an action on a resource
grants.can(["user"], "password", "change"); // true
grants.can(["admin"], "password", "change"); // false

// super role
grants.role("super").allow(new Permissions().resource("*").actions(["*"]));
```

For more usage details, refer to the [example](test/example.js)


## Browser
The module can also be used within the context of the browser. By default the Permissions and Grants classes use in-memory Store, and are suitable to build UI-frontends for creating/manipulating roles & permissions. Once ready, the frond-end can call the interfaces of the in-mem store to get the values and send it to the backend for persistence.


## Permissions API
A class that provides interfaces to create & manipulate the different resources and the actions that can be performed on the resources. All interfaces are synchronous, and the storage is in-memory. It allows to export and import (via constructor) to persist data.

#### Permissions([permissions])
Constructor that creates a permissions-object for creating/manipulating resources and related actions.    
Argument(s): Optional list of permissions-array previously exported    
Return-value: permissions-object

#### listResources()
Lists the available resources within the permissions-object.    
Argument(s): None    
Return-value: array of resource-strings

#### actions4resource(resource)
Lists the assigned actions to the specified resource in the permissions-object.    
Argument(s): resource-name    
Return-value: array of action-strings

#### export()
Exports (and returns) the list of available permissions in the object as a array-of-strings.    
Argument(s): None    
Return-value: array of permission-strings

#### can(resource, action)
Checks if a given action can be performed on a specified resource.    
Argument(s): resource-name, action    
Return-value: boolean

#### resource(resource)
Part of the fluent/chainable API to define a resource-name.    
Argument(s): resource-name, action    
Return-value: object (for chaining)

#### actions([actions])
Part of the fluent/chainable API to define actions on previously set resource-name.    
Argument(s): Array of actions    
Return-value: object (for chaining)

#### include(permissions-object)
Part of the fluent/chainable API to include additional permissions.    
Argument(s): permissions-object    
Return-value: object (for chaining)

#### exclude(permissions-object)
Part of the fluent/chainable API to exclude un-wanted permissions.    
Argument(s): permissions-object    
Return-value: object (for chaining)


## Grants API
A class that provides interfaces to create & manipulate the different roles and the associated permissions. Most of the interfaces are asynchronous, and provide both a callback and promise based interface. Data persistence is via the store-object passed to the constructor.

#### Grants(store)
Constructor that creates a grants-object for creating/manipulating roles and permissions.    
Argument(s): Store instance for persisting roles/permissions    
Return-value: grants-object

#### listRoles(callback)
Lists the available roles within the grants-object.    
Argument(s): callback or none    
Return-value: promise if callback omitted

#### permissions4role(role, callback)
Returns the available permissions for the specified role within the grants-object.    
Argument(s): callback or none    
Return-value: promise if callback omitted

#### deleteRole(role, callback)
Deletes the specified role within the grants-object and the store.    
Argument(s): callback or none    
Return-value: promise if callback omitted

#### can(roles, resource, action, callback)
Checks and returns a boolean if a given set of roles can perform the action on the specified resource.    
Argument(s): callback or none    
Return-value: promise if callback omitted

#### role(role)
Part of the fluent/chainable API to define a role.    
Argument(s): role-name    
Return-value: object (for chaining)

#### inherits(arr)
Part of the fluent/chainable API to inherit permissions from other pre-defined roles.    
Argument(s): array of role-names    
Return-value: object (for chaining)

#### allow(permsObj)
Part of the fluent/chainable API to allow additional permissions.    
Argument(s): permissions-object    
Return-value: object (for chaining)

#### deny(permsObj)
Part of the fluent/chainable API to deny un-wanted permissions.    
Argument(s): permissions-object    
Return-value: object (for chaining)

#### commit(callback)
Part of the fluent/chainable API to commit/save the role defintion to the store.    
Argument(s): callback or none    
Return-value: promise if callback omitted


## Store API
A class that provides interfaces to persist the different roles and the associated permissions. All the interfaces are asynchronous, and provide both a callback and promise based interface. The default implementation is a in-memory store, and can easily be extended to any of the DB stores.

#### getRoles(callback)
Returns the list of roles available in the store.    
Argument(s): callback or none    
Return-value: promise if callback omitted

#### getRolePermissions(role, callback)
Returns the list of perrmissions for a specified role available in the store.    
Argument(s): callback or none    
Return-value: promise if callback omitted

#### setRolePermissions(role, permissions, callback)
Assigns the list of perrmissions to a specified role in the store.    
Argument(s): callback or none    
Return-value: promise if callback omitted

#### deleteRole(role, callback)
Delets the specified role in the store.    
Argument(s): callback or none    
Return-value: promise if callback omitted

