/* globals describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint newline-per-chained-call: "off"*/

"use strict";

const assert = require("assert");
const lib = require("../index.js");
const Grants = lib.Grants;
const Permissions = lib.Permissions;
const Store = lib.Store;

var store = new Store(); // using default memory-store
var grants = new Grants(store);
var perms;


describe("Example", function () {
    it("should run without errors", () => {
        assert.doesNotThrow(async () => {
            // setup user-role
            perms = new Permissions();
            perms
                .resource("profile").actions(["read", "update"])
                .resource("password").actions(["verify", "update", "reset"]);
            await grants.role("user").allow(perms).commit();
            // setup admin-role
            perms = new Permissions();
            perms
                .resource("profile").actions(["create", "delete"])
                .resource("device").actions(["add", "delete"]);
            await grants.role("admin").inherits(["user"]).allow(perms).commit();
            // setup super-admin role
            perms = new Permissions();
            perms.resource("*").actions(["*"]);
            await grants.role("super").allow(perms).commit();
            // here is what to expect
            assert.ok(await grants.can(["user"], "profile", "read") === true);
            assert.ok(await grants.can(["user"], "profile", "create") === false);
            assert.ok(await grants.can(["admin"], "profile", "create") === true);
            assert.ok(await grants.can(["admin"], "password", "change") === false);
            assert.ok(await grants.can(["super"], "any-resource", "do-any-thing") === true);
            // clean-up
            await grants.deleteRole("user");
            await grants.deleteRole("admin");
            await grants.deleteRole("super");
        });
    });
});
