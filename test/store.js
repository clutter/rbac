/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/
/*eslint newline-per-chained-call: "off"*/

"use strict";

const assert = require("assert");
const lib = require("../index.js");
const Permissions = lib.Permissions;
const Store = lib.Store;

const data = {
    // resources: [actions]
    "password": ["change", "verify", "reset"],
    "profile": ["create", "read", "update", "delete"],
    "device": ["add", "delete"]
};
var store;
var adminPerms;
var userPerms;

before(function (done) {
    adminPerms = new Permissions();
    Object.keys(data).forEach(function (res) {
        adminPerms.resource(res).actions(data[res]);
    });
    userPerms = new Permissions();
    userPerms
        .resource("password").actions(data.password)
        .resource("profile").actions(["read", "update"]);
    store = new Store();
    done();
});

describe("Store", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof Store, "function");
        assert.ok(store instanceof Store);
        done();
    });
    it("should set roles & permissions", async () => {
        await store.setRolePermissions("admin", adminPerms.export());
        await store.setRolePermissions("user", userPerms.export());
    });
    it("should list roles", async () => {
        assert.deepEqual(await store.getRoles(), ["admin", "user"]);
    });
    it("should list role permissions", async () => {
        assert.deepEqual(await store.getRolePermissions("admin"), adminPerms.export());
        assert.deepEqual(await store.getRolePermissions("user"), userPerms.export());
    });
    it("should delete role", async () => {
        await store.deleteRole("admin");
        assert.deepEqual(await store.getRoles(), ["user"]);
        await store.deleteRole("user");
        assert.deepEqual(await store.getRoles(), []);
    });
});

after(function (done) {
    done();
});
