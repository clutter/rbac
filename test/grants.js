/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/
/*eslint newline-per-chained-call: "off"*/

"use strict";

const assert = require("assert");
const lib = require("../index.js");
const Grants = lib.Grants;
const Permissions = lib.Permissions;
const Store = lib.Store;

const data = {
    // resources: [actions]
    "password": ["change", "verify", "reset"],
    "profile": ["create", "read", "update", "delete"],
    "device": ["add", "delete"]
};
var store;
var grants;
var adminPerms;
var userPerms;
var restPerms;

before(function (done) {
    store = new Store();
    grants = new Grants(store);
    adminPerms = new Permissions();
    Object.keys(data).forEach(function (res) {
        adminPerms.resource(res).actions(data[res]);
    });
    userPerms = new Permissions();
    userPerms
        .resource("password").actions(data.password)
        .resource("profile").actions(["read", "update"]);
    restPerms = new Permissions();
    restPerms
        .resource("profile").actions(["create", "delete"])
        .resource("device").actions(data.device);
    done();
});

describe("Grants", function () {
    it("should be a class", (done) => {
        assert.strictEqual(typeof Grants, "function");
        assert.ok(grants instanceof Grants);
        done();
    });
    it("should throw without role", (done) => {
        assert.throws(() => {
            grants.inherits(["user"]);
        });
        assert.throws(() => {
            grants.allow(userPerms);
        });
        assert.throws(() => {
            grants.deny(userPerms);
        });
        done();
    });
    it("should throw if commit without role", async () => {
        try {
            await grants.commit();
        } catch (exp) {
            assert.ok(exp instanceof Error);
        }
    });
    it("should set roles & allow permissions", async () => {
        await grants.role("admin").allow(adminPerms).commit();
        await grants.role("user").allow(userPerms).commit();
    });
    it("should list roles", async () => {
        assert.deepEqual(await grants.listRoles(), ["admin", "user"]);
    });
    it("should list permissions for roles", async () => {
        assert.deepEqual(await grants.permissions4role("admin"), adminPerms.export());
        assert.deepEqual(await grants.permissions4role("user"), userPerms.export());
    });
    it("should delete roles", async () => {
        await grants.deleteRole("admin");
        assert.deepEqual(await grants.listRoles(), ["user"]);
        await grants.deleteRole("user");
        assert.deepEqual(await grants.listRoles(), []);
    });
    it("should inherit roles", async () => {
        await grants.role("user").allow(userPerms).commit();
        await grants.role("admin").inherits(["user"]).commit();
        assert.deepEqual(await grants.permissions4role("admin"), await grants.permissions4role("user"));
        await grants.deleteRole("admin");
        await grants.deleteRole("user");
        assert.deepEqual(await grants.listRoles(), []);
    });
    it("should set roles & deny permissions", async () => {
        await grants.role("admin").allow(adminPerms).commit();
        await grants.role("user").inherits(["admin"]).deny(restPerms).commit();
        assert.deepEqual(await grants.permissions4role("user"), userPerms.export());
    });
    it("should allow & disallow roles/permissions for resource/actions", async () => {
        await grants.role("super").allow(new Permissions().resource("*").actions("*")).commit();
        await grants.role("devadmin").allow(new Permissions().resource("device").actions("*")).commit();
        assert.ok(await grants.can(["super"], "password", "non-existent"));
        assert.ok(await grants.can(["super"], "non-existent", "verify"));
        assert.ok(await grants.can(["super"], "non-existent", "non-existent"));
        assert.ok(await grants.can(["user"], "password", "verify"));
        assert.ok(await grants.can(["user"], "profile", "update"));
        assert.ok(!await grants.can(["user"], "profile", "create"));
        assert.ok(!await grants.can(["user"], "device", "add"));
        assert.ok(await grants.can(["admin"], "device", "add"));
        assert.ok(!await grants.can(["admin"], "non-existent", "add"));
        assert.ok(!await grants.can(["admin"], "device", "non-existent"));
        assert.ok(await grants.can(["devadmin"], "device", "add"));
        assert.ok(await grants.can(["user", "devadmin"], "device", "add"));
        assert.ok(await grants.can(["user", "devadmin"], "profile", "update"));
        assert.ok(!await grants.can(["user", "devadmin"], "profile", "delete"));
        assert.ok(await grants.can(["user", "devadmin"], "device", "delete"));
        assert.ok(!await grants.can(["non-existent"], "device", "delete"));
        await grants.deleteRole("super");
        await grants.deleteRole("admin");
        await grants.deleteRole("devadmin");
        await grants.deleteRole("user");
        assert.deepEqual(grants.listRoles(), []);
    });
});

after(function (done) {
    done();
});
