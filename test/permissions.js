/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/
/*eslint  max-nested-callbacks: ["error", 6]*/

"use strict";

const assert = require("assert");
const lib = require("../index.js");
const Permissions = lib.Permissions;

const data = {
    // resources: [actions]
    "password": ["change", "verify", "reset"],
    "profile": ["read", "update", "delete"]
};
var perms;

before(function (done) {
    perms = new Permissions();
    done();
});

describe("Permissions", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof Permissions, "function");
        done();
    });
    it("should throw on adding badly-formatted resource", function (done) {
        assert.throws(function () {
            let p = new Permissions();

            p.resource("user%data");
        });
        assert.throws(function () {
            let p = new Permissions();

            p.resource("user data");
        });
        done();
    });
    it("should throw on adding actions without resource", function (done) {
        assert.throws(function () {
            let p = new Permissions();

            p.actions(data.password);
        });
        done();
    });
    it("should add multiple resources with actions", function (done) {
        assert.doesNotThrow(function () {
            Object.keys(data).forEach(function (res) {
                perms.resource(res).actions(data[res]);
            });
        });
        done();
    });
    it("should list resources", function (done) {
        assert.deepEqual(perms.listResources().sort(), Object.keys(data).sort());
        done();
    });
    it("should list actions for resource", function (done) {
        assert.deepEqual(perms.actions4resource("password").sort(), data.password.sort());
        assert.deepEqual(perms.actions4resource("profile").sort(), data.profile.sort());
        done();
    });
    it("should make, export & import permissions", function (done) {
        let arr = perms.export();
        let tmp = new Permissions(arr);

        assert.deepEqual(perms, tmp);
        assert.deepEqual(
            new Permissions()
                .resource("res")
                .actions(["act"])
                .export(),
            [Permissions.makeString("res", "act")]);
        done();
    });
    it("should include permissions", function (done) {
        let base = new Permissions();
        let tmp = new Permissions();

        assert.ok(base.resource("password").actions(data.password) instanceof Permissions);
        assert.ok(tmp.resource("profile").actions(data.profile) instanceof Permissions);
        assert.ok(base.include(tmp) instanceof Permissions);
        assert.deepEqual(base, perms);
        done();
    });
    it("should exclude permissions", function (done) {
        let base = new Permissions();
        let tmp = new Permissions();

        base.resource("password").actions(data.password);
        tmp.resource("profile").actions(data.profile);
        assert.ok(perms.exclude(tmp) instanceof Permissions);
        assert.deepEqual(base, perms);
        done();
    });
    it("should allow & disallow resource/actions", function (done) {
        let p1;
        let p2;

        p1 = new Permissions();
        p1.resource("*").actions(["*"]);
        assert.ok(p1.can("non-existent", "non-existent"));
        p1 = new Permissions();
        p1.resource("password").actions(["*"]);
        assert.ok(p1.can("password", "non-existent"));
        assert.ok(!p1.can("profile", "non-existent"));
        p1 = new Permissions().resource("password")
            .actions(data.password);
        p2 = new Permissions().resource("profile")
            .actions(data.profile);
        assert.ok(p1.can("password", data.password[0]));
        assert.ok(!p1.can("password", data.profile[0]));
        assert.ok(!p1.can("profile", data.password[0]));
        assert.ok(p2.can("profile", data.profile[0]));
        assert.ok(!p2.can("profile", data.password[0]));
        assert.ok(!p2.can("non-existent", data.profile[0]));
        assert.ok(!p2.can("profile", "non-existent"));
        done();
    });
});

after(function (done) {
    done();
});
