"use strict";

const promisify = require("util").promisify;

// store: [{role: "resource-name", perms: ["action-1", "action-2", ...]}]

var data = [];

function getRoles(callback) {
    process.nextTick(() => callback(null, data.map(x => x.role).sort()));
}

function getRolePermissions(role, callback) {
    process.nextTick(() => {
        let rec = data.find(function find(ele) {
            return ele.role === role;
        });

        callback(null, rec ? rec.perms : null);
    });
}

function setRolePermissions(role, permissions, callback) {
    process.nextTick(() => {
        let rec = data.find(function find(ele) {
            return ele.role === role;
        });

        if (rec) {
            rec.perms = permissions.sort();
        } else {
            data.push({
                "role": role,
                "perms": permissions.sort()
            });
        }
        callback(null);
    });
}

function deleteRole(role, callback) {
    process.nextTick(() => {
        let idx = data.findIndex(function find(ele) {
            return ele.role === role;
        });

        if (idx >= 0) { // eslint-disable-line no-magic-numbers
            data.splice(idx, 1); // eslint-disable-line no-magic-numbers
        }
        callback(null);
    });
}

class Store {
    getRoles(callback) {
        return callback ?
            getRoles(callback) :
            promisify(getRoles)();
    }
    getRolePermissions(role, callback) {
        return callback ?
            getRolePermissions(role, callback) :
            promisify(getRolePermissions)(role);
    }
    setRolePermissions(role, permissions, callback) {
        return callback ?
            setRolePermissions(role, permissions, callback) :
            promisify(setRolePermissions)(role, permissions);
    }
    deleteRole(role, callback) {
        return callback ?
            deleteRole(role, callback) :
            promisify(deleteRole)(role);
    }
}

module.exports = Store;
